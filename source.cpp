#include "header.hpp"

const static std::vector<char> staticVect = {1, 2, 3, 4};


const std::vector<char> &getVector() {
    return staticVect;
}
size_t getVectorGoodIdx() {
    return 2;
}
size_t getVectorBadIdx() {
    return 9;
}


void anti_optimizer_function(const std::vector<char>&) {}
void anti_optimizer_function(const char&) {}
void anti_optimizer_function() {}
