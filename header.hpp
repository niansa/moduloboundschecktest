#ifndef _HEADER_HPP
#define _HEADER_HPP
#include <vector>
#include <cstddef>


const std::vector<char> &getVector();
size_t getVectorGoodIdx();
size_t getVectorBadIdx();

// Prevents the compiler from detecting the final producs as unused which way it may skip creating it in first place
void anti_optimizer_function(const std::vector<char>&);
void anti_optimizer_function(const char&);
void anti_optimizer_function();
#endif // _HEADER_HPP
