#include "header.hpp"
#include "Timer.hpp"

#include <iostream>
#include <chrono>
#include <array>



int main() {
    // Settings
    constexpr unsigned repeats = 100000000,
                       main_repeats = 8,
                       heatups = 2;
    std::cout << "\nEach test in set is repeated " << repeats << " times and the whole set is repeated " << main_repeats << " times plus " << heatups << " times heatup.\n" << std::endl;

    // Collection of results
    std::array<unsigned long, 4> results = {0, 0, 0, 0};

    // Measure a couple of times
    unsigned long duration;
    Timer timer;
    const auto& vec = getVector();
    const auto vecGoodIdx = getVectorGoodIdx();
    const auto vecBadIdx = getVectorBadIdx();
    for (unsigned i = 0; i != main_repeats+heatups; i++) {
        const bool inHeatup = i < heatups;

        // Test #1
        std::cout << "Running test #1: By BoundsCheckViaModuloWithGoodIdx" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            anti_optimizer_function(vec[vecGoodIdx % vec.size()]);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (inHeatup) {
            results[0] += duration;
        }

        // Test #2
        std::cout << "Running test #2: By BoundsCheckViaBranchWithGoodIdx" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            if (vec.size() > vecGoodIdx) {
                anti_optimizer_function(vec[vecGoodIdx]);
            } else {
                anti_optimizer_function();
            }
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (inHeatup) {
            results[1] += duration;
        }

        // Test #3
        std::cout << "Running test #1: By BoundsCheckViaModuloWithBadIdx" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            anti_optimizer_function(vec[vecBadIdx % vec.size()]);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (inHeatup) {
            results[2] += duration;
        }

        // Test #4
        std::cout << "Running test #2: By BoundsCheckViaBranchWithBadIdx" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            if (vec.size() > vecBadIdx) {
                anti_optimizer_function(vec[vecBadIdx]);
            } else {
                anti_optimizer_function();
            }
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (inHeatup) {
            results[3] += duration;
        }
    }

    // Print final results calculating average
    std::cout << "\nTests have finished.\n"
                 "Average time spent by BoundsCheckViaModuloWithGoodIdx: " << (results[0]/main_repeats) << "ms\n"
                 "Average time spent by BoundsCheckViaBranchWithGoodIdx: " << (results[1]/main_repeats) << "ms\n"
                 "Average time spent by BoundsCheckViaModuloWithBadIdx: " << (results[2]/main_repeats) << "ms\n"
                 "Average time spent by BoundsCheckViaBranchWithBadIdx: " << (results[3]/main_repeats) << "ms\n";
}
